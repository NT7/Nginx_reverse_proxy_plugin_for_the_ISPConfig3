#!/bin/bash

WORKING_DIR="/usr/local/src/nginx"
NGINX_ETC="/etc/nginx"

NGINX_VERSION="1.9.2"
OPENSSL_VERSION="1.0.2d"
PAGESPEED_VERSION="1.9.32.4"
HEADERS_MORE_VERSION="0.26"
CACHE_PURGE_VERSION="2.3"

CONFIGURE="--add-module=${WORKING_DIR}/ngx_pagespeed-release-${PAGESPEED_VERSION}-beta \
--add-module=${WORKING_DIR}/headers-more-nginx-module-${HEADERS_MORE_VERSION} \
--add-module=${WORKING_DIR}/ngx_cache_purge-${CACHE_PURGE_VERSION} \
--prefix=/usr/share/nginx --sbin-path=/usr/sbin/nginx --conf-path=${NGINX_ETC}/nginx.conf \
--pid-path=/var/run/nginx.pid --lock-path=/var/lock/nginx.lock \
--error-log-path=/var/log/nginx/error.log --http-log-path=/var/log/access.log \
--user=www-data --group=www-data \
--without-http_autoindex_module --without-http_geo_module --without-http_map_module --without-http_empty_gif_module \
--without-http_referer_module --without-http_memcached_module --without-http_scgi_module --without-http_browser_module \
--without-http_upstream_ip_hash_module --without-http_upstream_least_conn_module --without-http_upstream_keepalive_module \
--without-http_split_clients_module --without-http_userid_module --without-http_uwsgi_module \
--without-mail_pop3_module --without-mail_imap_module --without-mail_smtp_module \
--with-ipv6 --with-http_ssl_module --with-http_spdy_module --with-openssl=${WORKING_DIR}/openssl-${OPENSSL_VERSION} \
--with-pcre --with-pcre-jit --with-threads"


# Download + Unpack
# ------------------------------------------
mkdir -p ${WORKING_DIR}

# Nginx
cd ${WORKING_DIR}
wget http://nginx.org/download/nginx-${NGINX_VERSION}.tar.gz
tar xzvf nginx-${NGINX_VERSION}.tar.gz
# OpenSSL
cd ${WORKING_DIR}
wget ftp://mirror.switch.ch/mirror/openssl/source/openssl-${OPENSSL_VERSION}.tar.gz --no-check-certificate
tar xzvf openssl-${OPENSSL_VERSION}.tar.gz
# PageSpeed
cd ${WORKING_DIR}
wget https://github.com/pagespeed/ngx_pagespeed/archive/release-${PAGESPEED_VERSION}-beta.zip
unzip release-${PAGESPEED_VERSION}-beta.zip
cd ngx_pagespeed-release-${PAGESPEED_VERSION}-beta/
wget https://dl.google.com/dl/page-speed/psol/${PAGESPEED_VERSION}.tar.gz
tar xzvf ${PAGESPEED_VERSION}.tar.gz
# Headers
cd ${WORKING_DIR}
wget https://github.com/agentzh/headers-more-nginx-module/archive/v${HEADERS_MORE_VERSION}.tar.gz
tar xzvf v${HEADERS_MORE_VERSION}.tar.gz
# Cache
cd ${WORKING_DIR}
wget http://labs.frickle.com/files/ngx_cache_purge-${CACHE_PURGE_VERSION}.tar.gz
tar xzvf ngx_cache_purge-${CACHE_PURGE_VERSION}.tar.gz


# Compiling
# ------------------------------------------
cd ${WORKING_DIR}/nginx-${NGINX_VERSION}
./configure ${CONFIGURE}
make


# Prepare for installation + install
# ------------------------------------------
mv ${NGINX_ETC} ${NGINX_ETC}_old
service nginx stop
make install

cp ${NGINX_ETC}/koi-utf ${NGINX_ETC}_old/koi-utf
cp ${NGINX_ETC}/koi-win ${NGINX_ETC}_old/koi-win
cp ${NGINX_ETC}/mime.types ${NGINX_ETC}_old/mime.types
cp ${NGINX_ETC}/mime.types.default ${NGINX_ETC}_old/mime.types.default
cp ${NGINX_ETC}/win-utf ${NGINX_ETC}_old/win-utf
# PageSpeed libraries conf
${WORKING_DIR}/ngx_pagespeed-release-${PAGESPEED_VERSION}-beta/scripts/pagespeed_libraries_generator.sh > ${NGINX_ETC}_old/conf.d/pagespeed_libraries.conf

rm -rf ${NGINX_ETC}
mv ${NGINX_ETC}_old ${NGINX_ETC}
chown -R root:root ${NGINX_ETC}
service nginx start


# Cleanup
# ------------------------------------------
rm -rf ${WORKING_DIR}


# Confirm installation
# ------------------------------------------
nginx -V
